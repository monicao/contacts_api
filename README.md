# Contact List API

LHL Demo for using jQuery to build a client-side application.

To run the server:

```
bundle install
bin/rake db:reset
bin/rails s
```

Visit `http://localhost:3000/app.html` for one implementation.

Visit `http://localhost:3000/app_with_ejs.html` to see the implementation that uses [ejs](http://www.embeddedjs.com/) client-side templates