- demo of working with API
  - what is an API? (definition)
  - how to structure handlers
  - how to refactor code to DRY it up
  - how to debug requests
  - diff betwee $.get (*/*) and $.getJSON
- CORS
- $("selector").data()
- RFCs



With $.get() the response is treated as a string.

// HTTP Request content type header will be "*/*"
$.get("/api/contacts").success(function(data) {
  var contacts = JSON.parse(data)
})

// HTTP Request content type header will be "application/json"
$.getJSON("/api/contacts").success(function(data) {
    
})