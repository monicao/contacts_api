// Document Ready - Wait for the DOM to load

$(function(){

  var templates = {
    contactDetails: new EJS({url: '/ejs/contact_details.ejs'}),
    showContacts: new EJS({url: '/ejs/show_contacts.ejs'})
  }

  var dataStore = {
    contacts: []
  }


  // Example of using a jQuery data attribute
  // Use a delegate click listener to bind the click
  // event to a dom element that doesn't exist on the page yet.
  // $(".container").on("click", "li", function(){
  //   var contact = $(this).data("monkey")
  //   uiHandlers.showContactDetail(contact)
  // })

  var uiHandlers = {
    showContactDetail: function(e) {
      // var title = $("<h2>").text(contact.name)
      // var email = $("<p>").text(contact.email)
      // $(".container").empty().append(title).append(email)
      var contact = JSON.parse(unescape($(this).data("contact")))
      var contactDetailsHTML = templates.contactDetails.render({contact: contact})
      $(".container").replaceWith(contactDetailsHTML)

    },
    // Render the contacts page
    // contacts - Array of contact objects
    showContacts: function(contacts) {
      var self = this
      // // This function is called if the HTTP response is a 2xx response
      // var list = $("<ul>")
      // contacts.forEach(function(contact){
      //   // the .data() call writes the contact object to the <li> DOM elmement 
      //   // as a data attribute that we can retrieve later
      //   var contactLi = $("<li>").text(contact.name).data("monkey", contact) 
      //   contactLi.click(function(e) {
      //     this.showContactDetail(contact)
      //   })
      //   list.append(contactLi)
      // })
      // var addContactForm = $("<form>").attr("class", "add-contact").submit(this.addContact)
      // var nameField = $("<input>").attr("type", "text").attr("name", "name").attr("class", "contact-name").attr("placeholder", "Name")
      // var emailField = $("<input>").attr("type", "text").attr("name", "email").attr("class", "contact-email").attr("placeholder", "Email")
      // var submitButton = $("<input>").attr("type", "submit")
      // addContactForm.append(nameField).append(emailField).append(submitButton)

      // Instead of creating the html manually with jquery, we can render the ejs template instead
      // The render method uses the template showContacts.ejs and passes the contacts as a variable
      // to the template
      var showContactsHTML = templates.showContacts.render({contacts: contacts})

      // we will need to set the event listeners manually
      // first we convert the HTML string to a chunk of DOM that jQuery can work with
      showContactsHTML = $(showContactsHTML)
      // now we can run jQuery methods like .find()
      showContactsHTML.find("li").on("click", this.showContactDetail)
      showContactsHTML.find("form.add-contact").on("submit", this.addContact)
      $(".container").replaceWith(showContactsHTML)
    },
    addContact: function(e) {
      var self = this
      e.preventDefault()
      var contactData = {
        contact: {
          name: $(this).find("input.contact-name").val(),
          email: $(this).find("input.contact-email").val()
        }
      }
      $.ajax({
        url: "http://localhost:3000/api/contacts",
        method: "post",
        data: contactData,
        dataType: "json"
      })
      .done(function(contact, foo){
        dataStore.contacts.push(contact)
        uiHandlers.showContacts(dataStore.contacts)
      })
      .fail(function(response, data){
        console.log(response)
        alert("Could not submit the form.")
      })
    }
  }

  $.getJSON("http://localhost:3000/api/contacts")
  .done(function(contacts){
    uiHandlers.showContacts(contacts)
    dataStore.contacts = contacts
  })
  .fail(function(response){
    // 500 ... // 401    // 404
    switch(response.status){
      case 500:
        alert("There is a problem with the server")
        break;
      default:
        alert("There is some other problem")
    }
  })

})