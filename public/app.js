// Document Ready - Wait for the DOM to load

$(function(){

  var dataStore = {
    contacts: []
  }


  // Example of using a jQuery data attribute
  // Use a delegate click listener to bind the click
  // event to a dom element that doesn't exist on the page yet.
  // $(".container").on("click", "li", function(){
  //   var contact = $(this).data("monkey")
  //   uiHandlers.showContactDetail(contact)
  // })

  var uiHandlers = {
    showContactDetail: function(contact) {
      var title = $("<h2>").text(contact.name)
      var email = $("<p>").text(contact.email)

      $(".container").empty().append(title).append(email)

    },
    // Render the contacts page
    // contacts - Array of contact objects
    showContacts: function(contacts) {
      var self = this
      // This function is called if the HTTP response is a 2xx response
      var list = $("<ul>")
      contacts.forEach(function(contact){
        // the .data() call writes the contact object to the <li> DOM elmement 
        // as a data attribute that we can retrieve later
        var contactLi = $("<li>").text(contact.name).data("monkey", contact) 
        contactLi.click(function(e) {
          this.showContactDetail(contact)
        })
        list.append(contactLi)
      })

      var addContactForm = $("<form>").attr("class", "add-contact").submit(this.addContact)
      var nameField = $("<input>").attr("type", "text").attr("name", "name").attr("class", "contact-name").attr("placeholder", "Name")
      var emailField = $("<input>").attr("type", "text").attr("name", "email").attr("class", "contact-email").attr("placeholder", "Email")
      var submitButton = $("<input>").attr("type", "submit")
      addContactForm.append(nameField).append(emailField).append(submitButton)

      $(".container").empty().append(list).append(addContactForm)
    },
    addContact: function(e) {
      var self = this
      e.preventDefault()
      var contactData = {
        contact: {
          name: $(this).find("input.contact-name").val(),
          email: $(this).find("input.contact-email").val()
        }
      }
      $.ajax({
        url: "http://localhost:3000/api/contacts",
        method: "post",
        data: contactData,
        dataType: "json"
      })
      .done(function(contact, foo){
        dataStore.contacts.push(contact)
        debugger
        uiHandlers.showContacts(dataStore.contacts)
      })
      .fail(function(response, data){
        console.log(response)
        alert("Could not submit the form.")
      })
    }
  }

  $.getJSON("http://localhost:3000/api/contacts")
  .done(function(contacts){
    uiHandlers.showContacts(contacts)
    dataStore.contacts = contacts
  })
  .fail(function(response){
    // 500 ... // 401    // 404
    switch(response.status){
      case 500:
        alert("There is a problem with the server")
        break;
      default:
        alert("There is some other problem")
    }
  })

})